# Complementary material

January 2019 - v1.0  
Romain Jacob - jacobr@ethz.ch

This repository contains additional materials related to the following paper
> *Towards a Methodology for Experimental Evaluation in Low-Power Wireless Networking,*  
Romain Jacob, Carlo A. Boano, Usman Raza, Marco Zimmerling, Lothar Thiele,  
2nd Workshop on Benchmarking Cyber-Physical Systems and Internet of Things (CPS-IoTBench'19), April 2019.  
[Direct Link](http://www.romainjacob.net/wp-content/uploads/preprint_CPS-IoTBench19.pdf)

This file serves as top-level documentation for the complete artefacts (including experimental raw data, processing scripts, and final figures). Additional documentation is available in the individual artefacts whenever relevant.

A list of files is included at the end of this README, with a short description for each.

## Data Processing Tool Chain

The processing tool chain should in principal work out-of-the-box for any UNIX-based OS.

**Requirements**
- Bash shell
- Matlab
- Python

**Tested configurations**
- Microsoft Windows 10, Matlab R2018b, Python 3.7.2
- Ubuntu 16.04 LTS, 	Matlab R2018b, Python 2.7.12
- Ubuntu 18.04.01 LTS, 	Matlab R2018b, Python 2.7.15


The artefact and processing scripts are divided in three steps.  
Step I.   lets you visualize the data that we have collected and processed.  
Step II.  enables you to re-do the processing based on the raw data we collected.  
Step III. explains how to re-do the complete experiments.

### 1. Visualization of the data presented in the paper
- [Matlab] Load the matlab workspace `workspace_evaluation.mat` to explore the data.
- [Matlab] In Matlab, open any `/plots/*.fig` file to open and explore the graphs.

### 2. Re-run the data processing
- [Bash]   Run `extract_PRR.sh`            ->  Extract PRR information from Flocklab test results 
- [Matlab] Run `compute_all_metrics.m`     ->  Compute all metrics and performance indicators  
Set `do_plots = 1` to enable plotting  ( this produces a lot of raw plots, fairly undocumented )
- [Matlab] Run `repeatability_Test_all.m`  ->  Compute bootstrap distribution of performance indicators  
Set `do_plots = 1` to enable plotting

### 3. Re-run the experiments
- Delete all test directories in the `/10x` folders.
- Run `generate_flocklab_tests.sh` script to schedule one test for each protocol (requires a Flocklab user account -- http://flocklab.ethz.ch). The tests IDs are saved in a newly created `test_summary.lst` file.
- Run `fetch_flocklab_tests.sh` to download the relevant test results. This scripts calls `flocklab2metric_sink_022.py`, which does some initial processing (compute e.g. the PRR for one test) and stores the results in `/10x/<test_folder>/KPI.txt`.

## List of files

| File name | Short Description |
| --- | --- |
|./10X                         |  Directory containing data related to protocol X|
|./plots                       |  Directory containing all plots shown in the paper - Contains both pdf (static) and matlab (dynamic) figures|
| | |
|compute_all_metrics.m           |Matlab script that computes all metrics for all protocols|
|compute_Perf_Indicators.m       |Matlab function that computes the performance indicators of a set of protocol results|
|expected_data.lst               |List of pseudo-random payload expected to be received at the sink node (used by `flocklab2metric_sink_022.py`)|
|extract_PRR.sh                  |Bash script that extracts necessary PRR information from Flocklab test results|
|fetch_flocklab_tests.sh         |Bash script that fetches the results of all Flocklab test listed in `test_summary.lst` (file created when by `generate_flocklab_tests.sh`)|
|flocklab                        |Command line tool for Flocklab|
|flocklab2metric_sink_022.py     |Python script that computes basic metrics on Flocklab test results (assumes that node 022 is the sink)|
|flocklab_template.xml           |Flocklab XML template|
|generate_flocklab_tests.sh      |Bash script that prepares the Flocklab test configuration file and schedules one test of each protocol|
|LICENSE	                     |License information for the files in this repository|
|plot_details.m                  |Matlab script that produces the detailed plot showing the PRR and average energy consumption of one protocol|
|README.txt                      |This file|
|repeatability_Test_all.m        |Matlab script that calls repeatability_Test() in a loop|
|repeatability_Test.m            |Matlab function that investigates the repeatability of one protocol using the bootstrapping technique|
|workspace_evaluation.mat        |Matlab workspace containing the processed data and results from the evaluation|
