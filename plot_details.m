% Copyright (c) 2018-2019, Swiss Federal Institute of Technology (ETH Zurich)
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution.
% 
% * Neither the name of the copyright holder nor the names of its
%   contributors may be used to endorse or promote products derived from
%   this software without specific prior written permission.
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

%--------------------------------------------------------------------------
% Processing script - Plot detailed data
%--------------------------------------------------------------------------
% CPS-IoTBench workshop paper
% Romain Jacob, January 2019
%--------------------------------------------------------------------------

clc
close all

% LB and UB for 95% CI on median
global UB LB;

%%

% Get data for selected protocol
config      = 1;
protocol    = 3;

Current_perNode{config}{protocol};
PRR_all{config}{protocol};
Medians = median(Current_perNode{config}{protocol},2);

figure 

% Draw the PRR line distribution
g = sort(PRR_all{config}{protocol});
y = ones(1,numel(g));
f = scatter(subplot(2,1,1),g,y,'filled','ok');
hold on
% Draw the 95% confidence interval for the median
plot(subplot(2,1,1), [g(LB) g(UB)], [2 2], 'Color',[255,99,46]/255);
plot(subplot(2,1,1), g(LB), 2,'s', 'Color',[255,99,46]/255, 'MarkerFaceColor',[255,99,46]/255);
ylim([0 3]);
xlim([63 102]);
set(gca,'ytick',[])
set(gca,'ycolor','none')
xlabel('PRR (%)') ;
hold off

% Draw the current line distribution
g = sort(Medians);
y = ones(1,numel(g));
f = scatter(subplot(2,1,2),g,y,'filled','ok');
hold on
% Draw the 95% confidence interval for the median
plot(subplot(2,1,2), [g(LB) g(UB)], [2 2], 'Color',[255,99,46]/255);
plot(subplot(2,1,2), g(UB), 2,'s', 'Color',[255,99,46]/255, 'MarkerFaceColor',[255,99,46]/255);
ylim([0 3]);
xlim([2.63 2.82]);
set(gca,'xtick',2.65:0.05:2.8);
set(gca,'ytick',[])
set(gca,'ycolor','none')
xlabel('Median Current Draw (mA)') ;
hold off

x0=10;
y0=10;
width=500;
height=200;
set(gcf,'position',[x0,y0,width,height])

saveas(gcf,'plots/examplePerf.fig')
saveas(gcf,'plots/examplePerf.pdf')

%plotly_fig = fig2plotly(gcf, 'filename', 'examplePerf');
