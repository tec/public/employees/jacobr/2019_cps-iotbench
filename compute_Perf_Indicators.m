function [X] = compute_Perf_Indicators(Current, PRR)
%compute_Perf_Indicators Compute aggregated performance indicators for a
%given protocol and a given configuration.
%
%   Each row of the inputs contains the PRR and Current draw for one test.
%   - PRR is a vector, value are in percentage.
%   - Current is a matrix where each column contains the average current 
%   measure for one node (in mA).
%
%   Output metrics are normalized, adimensionnal, and defined such that
%   higher values are better. 1 is best, 0 is worst.
%
%   X(1) : Average Energy
%   X(2) : Worst-case Energy
%   X(3) : Reliability


% Output performance vector (row-vector)
X = zeros(1,3);

% Helpers
nTests = size(Current,1);
nNodes = size(Current,2);

% LB and UB for 95% CI on median
global UB LB;

%% Average Energy
% UB of the 95% CI on the median across tests  
   
    % Compute and store the median CI for each test
    medianPerTest = zeros(nTests,1);
    for i=1:nTests 
        tmp = sort(Current(i,:));
        medianPerTest(i) = median(tmp);
    end

    % Store the performance indicator value
    medianPerTest = sort(medianPerTest);
    X(1) = 1-medianPerTest(UB)/25; 


%% Worst-case Energy

    % Compute the median CI for each node
    max_value = 0;
    for i=1:nNodes
        tmp = sort(Current(:,i));
        if tmp(UB) > max_value                
            max_value = tmp(UB);
        end
    end

    % Store the performance indicator value
    X(2) = 1-max_value/25; 

%% Reliability
% LB of the 95% CI on the median across tests

    % Store the performance indicator value
    PRR = sort(PRR);
    X(3) = PRR(LB)/100; 
end
