#!/bin/bash

# Copyright (c) 2018-2019, Swiss Federal Institute of Technology (ETH Zurich)
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
# 
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
# 
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if [ $# -ne 2 ]
then
  echo 'usage: ./fetch_flocklab_tests USERNAME PASSWORD'
  exit 1;
fi

curl_site='https://www.flocklab.ethz.ch/user/webdav/'
scratch=/tmp/flocklab
maxtime=1800

# Iterate over all tests
for t in `sed '/^#/d;s/#.*//' test_summary.lst | awk '{print $6}' | sed 's/,//g'`
do
  fail=0
  if [ ! -d $scratch ]
  then
          mkdir $scratch
  fi
  starttime=`date +%s`
  duration=$((`date +%s` - $starttime))

  # Download results from FlockLab
  while [ $duration -lt $maxtime ]
  do
    curl --user $1:$2 $curl_site$t/results_nopower.tar.gz --output $scratch/results.tar.gz
    curl --user $1:$2 $curl_site$t/testconfiguration.xml --output $scratch/testconfiguration.xml
    duration=$maxtime
  done

  # Unpack results
  path=`pwd`
  cd $scratch
  gunzip $scratch/results.tar.gz
  tar --exclude='errorlog*' --exclude='powerprofiling.csv' --exclude='gpiotracing*' --exclude='gpioactuation*' -xvf $scratch/results.tar

  # Extract test information
  cd $path
  group=`sed '/GROUP=/!d;s/.*=\([0-9]*\).*/\1/' $scratch/testconfiguration.xml`
  datarate=`sed '/DATARATE=/!d;s/.*=\([0-9]*\).*/\1/' $scratch/testconfiguration.xml`
  sinkaddress=`sed '/SINK=/!d;s/.*=\([0-9]*\).*/\1/' $scratch/testconfiguration.xml`
  echo $group $datarate $sinkaddress

  # Evaluate performance metrics
  if [ $sinkaddress -eq 031 ]
  then
    ./flocklab2metric_sink_031.py $sinkaddress $scratch/$t/serial.csv $scratch/$t/powerprofilingstats.csv > $scratch/$t/KPI.txt
  fi

  if [ $sinkaddress -eq 022 ]
  then
    ./flocklab2metric_sink_022.py $sinkaddress $scratch/$t/serial.csv $scratch/$t/powerprofilingstats.csv > $scratch/$t/KPI.txt
  fi

  if [ $sinkaddress -eq 025 ]
  then
    ./flocklab2metric_sink_025.py $sinkaddress $scratch/$t/serial.csv $scratch/$t/powerprofilingstats.csv > $scratch/$t/KPI.txt
  fi

  # Move results into folder
  resultfolder=$group/${t}_dr${datarate}_sink${sinkaddress}
  mkdir $resultfolder
  cp $scratch/$t/* $resultfolder

  # Clean-up
  rm -rf $scratch

done
