% Copyright (c) 2018-2019, Swiss Federal Institute of Technology (ETH Zurich)
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution.
% 
% * Neither the name of the copyright holder nor the names of its
%   contributors may be used to endorse or promote products derived from
%   this software without specific prior written permission.
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

%--------------------------------------------------------------------------
% Processing script - Repeatability test
%--------------------------------------------------------------------------
% CPS-IoTBench workshop paper
% Romain Jacob, January 2019
%--------------------------------------------------------------------------

function [bootstat] = repeatability_Test(config, protocol, do_plots, Current_perNode, PRR_all)
%repeatability_Test Compute a population of 10000 bootstrap samples for a
%given protocol/congifuratino, and returns the performance vector for each
%sample.
%
%   If do_plots is set to 1, the 3D scatter plot of performance vectors is
%   produced.
%
%   bootstat(i,:) contains the vector of performance indicators for the
%   i-th bootstrap sample.

nIndicators = 3;

[bootstat,bootsam] = bootstrp(10000, @compute_Perf_Indicators, Current_perNode{config}{protocol},PRR_all{config}{protocol});

%% Optional logging of the bootstrapping sample
% csvwrite(strcat('Repeatability',num2str(protocol),'.csv'),bootstat);

%% Approach based on CI
% Compute and print different CI for each indicator
fprintf('\n%s\n', '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
fprintf('%s\t\t%s\n', 'Configuration', num2str(config));
fprintf('%s\t\t%s\n', 'Protocol', num2str(protocol));  
fprintf('%s\n', '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');

originalPerfIndicators = compute_Perf_Indicators(Current_perNode{config}{protocol},PRR_all{config}{protocol});
perfAspects = { 'Average current draw ', ...
                'Worst current draw   ', ...
                'Average PRR          '};
    
for i=1:nIndicators
    fprintf('%s:\t%2.2f\n', perfAspects{i}, originalPerfIndicators(i));
    Y = prctile(bootstat(:,i),[2.5 5 12.5 87.5 95 97.5]);
    fprintf("95%%CI: %2.2f - %2.2f -> %2.2f \n",Y(1), Y(6), Y(6) - Y(1));
    fprintf("90%%CI: %2.2f - %2.2f -> %2.2f \n",Y(2), Y(5), Y(5) - Y(2));
    fprintf("75%%CI: %2.2f - %2.2f -> %2.2f \n\n",Y(3), Y(4), Y(4) - Y(3));
end

if do_plots
    figure
    scatter3(originalPerfIndicators(1),originalPerfIndicators(2),originalPerfIndicators(3),'filled','d')
    hold on
    scatter3(bootstat(:,1),bootstat(:,2),bootstat(:,3));
    xlim([0 1]);
    ylim([0 1]);
    zlim([0 1]);
    xlabel('Average Energy');
    ylabel('Maximal Energy');
    zlabel('Reliability');
    x0=10;
    y0=10;
    width=350;
    height=300;
    set(gcf,'position',[x0,y0,width,height])
    saveas(gcf,strcat('plots/repeatability',num2str(protocol),'.fig'))
    saveas(gcf,strcat('plots/repeatability',num2str(protocol),'.pdf'))
    hold off
end

end