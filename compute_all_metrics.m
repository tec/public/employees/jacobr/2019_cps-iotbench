% Copyright (c) 2018-2019, Swiss Federal Institute of Technology (ETH Zurich)
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution.
% 
% * Neither the name of the copyright holder nor the names of its
%   contributors may be used to endorse or promote products derived from
%   this software without specific prior written permission.
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

%--------------------------------------------------------------------------
% Processing script - Compute all metrics
%--------------------------------------------------------------------------
% CPS-IoTBench workshop paper
% Romain Jacob, January 2019
%--------------------------------------------------------------------------

clear all;
close all;
clc;

format long;

% Enable/disable plots
do_plots = 0;

% Select the protocol results to process
protocol= {'101','102','103','104','105','106','107'}; 
%   Identifies the protocol number
configuration= {'022'};
%   Identifies the sink node ID (can be patched in the protocol binary files)
%   Node 22 is the currently used sink node. Do not change unless you know
%   what you are doing.

% Define path to FlockLab logs
path = pwd();

% LB and UB for 95% CI on median
% -> Depends on the number of runs performed (for us: N=20)
% -> Values from tables: http://www.schmid-werren.ch/hanspeter/publications/2014sscm.pdf
global UB LB;
UB = 15;
LB = 6;     

%% Storing elements
global Current_perNode Current_all PRR_all PerfIndicators;

%   For all storing elements, {i}{j} contains the data for
%      configuration   i
%      protocol        j

PerfIndicators  = {};
%   PerfIndicators{i}{j}(:,1) contains the original performance indicators
%   PerfIndicators{i}{j}(:,2) contains the normalized ones
%   PerfIndicators{i}{j}(1,:) contains PRR data
%   PerfIndicators{i}{j}(2,:) contains average current draw data
%   PerfIndicators{i}{j}(3,:) contains worst-case current draw data

PRR_all         = {};
%   PRR_all{i}{j} is a vector, one value per test. Value are in percentage.

Current_perNode = {};
%   Current_perNode{i}{j} is a matrix, value are in mA.
%   Current_perNode{i}{j}(k,:) contains the average current for each node
%                               for the k-th test.
%   Current_perNode{i}{j}(:,k) contains the average current for each test
%                               for the k-th node.

Current_all     = {};
%   Current_all{i}{j} is a cell array.
%   Current_all{i}{j}{1,k} is a matrix where rows contain [node_id, current draw]
%   Current_all{i}{j}{2,k} contains the median current draw for test k

%% Extract all PRR information

if do_plots
    figure
end

for i=1:numel(configuration)
    for j=1:numel(protocol)
    
        csvfile_single = strcat(path, '/', protocol{j}, '/PRR_sink', configuration{i}, '.csv');
        matfile_single = strcat(path, '/', protocol{j}, '/PRR_sink', configuration{i}, '.mat');

        if (~exist(matfile_single, 'file'))
            g = csvread(csvfile_single);
            save(matfile_single, 'g');
        else
            load(matfile_single);
        end
        
        PRR_all{i}{j} = g;
        tmp = sort(g);        
        
        % Store PRR metric in perf_results
        PerfIndicators{i}{j}(1,1) = tmp(LB);      % LB of the 95% CI on the median across tests
        PerfIndicators{i}{j}(1,2) = tmp(LB)/100;  % Normalized performance index

        if do_plots
            % Draw the line distribution
            y = ones(1,numel(g));
            f = scatter(subplot(7,1,j),g,y,'filled','d');
            hold on
            % Draw the 95% confidence interval for the median
            plot(subplot(7,1,j), [tmp(LB) tmp(UB)], [0.5 0.5], '-s','MarkerFaceColor','r');
            xlim([0 100]);
            ylim([0 1.5]);
            ylabel(protocol{j});
            hold off
        end
    end
end

%% Extract all current draw information

for i=1:numel(configuration)
    for j=1:numel(protocol)
    
        % Get the list of directories
        test_paths = dir(strcat(path, '/', protocol{j}));
        test_count = 0;
        % Remove /. and /..
        test_paths(1:2) = [];
        dirFlags = [test_paths.isdir];
        test_paths = test_paths(dirFlags);
        
        for k=1:numel(test_paths)
            % Match against the configuration
            if regexp(test_paths(k).name, strcat('sink', configuration{i}))
                test_count = test_count+1;
                % Extract current draw
                csvfile_single = strcat(test_paths(k).folder, '/', test_paths(k).name, '/powerprofilingstats.csv');
                g = csvread(csvfile_single,1,1);

                % Store all 
                Current_all{i}{j}{1,test_count} = g;
                
                % Compute the median and store 
                Current_all{i}{j}{2,test_count} = median(g(:,2));
                tmp_stor(test_count) = median(g(:,2));
            end
        end
        
        if do_plots
            % Draw the line distribution
            y = ones(1,numel(tmp_stor));
            f = scatter(subplot(7,1,j),tmp_stor,y,'filled','d');
            hold on
            % Draw the 95% confidence interval for the median
            LB = 1;
            UB = 6;
            sorted = sort(tmp_stor);
            plot(subplot(7,1,j), [sorted(LB) sorted(UB)], [0.5 0.5], '-s','MarkerFaceColor','r');
            xlim([0 5]);
            ylim([0 1.5]);
            ylabel(protocol{j});
        end
        
    end
    
    if do_plots
        xlabel('Median current draw [mA]');
        hold off
    end
end

if do_plots
    % Draw all data points in one scatter plot per protocol
    for i=1:numel(configuration)
        figure 
        for j=1:numel(protocol)
            x = [];
            y = [];
            for k=1:size(Current_all{i}{j},2) % for each test
               x = [x ; ones(size(Current_all{i}{j}{1,k},1),1)*k];
               y = [y ; Current_all{i}{j}{1,k}(:,2)];
            end
            scatter(subplot(7,1,j),x,y,'filled','d');
            ylabel(protocol{j});
            xlim([0.5 k+0.5]);
            ylim([0 25]);
        end
        xlabel('Different runs');
    end
end

%% Investigation of average current draw

for i=1:numel(configuration)
    
    for j=1:numel(protocol)        
        
        % Allocate temporary array
        tmp = zeros(size(Current_all{i}{j},2), 1);
        
        for k=1:size(Current_all{i}{j},2) % for each test            
            % Get median current draw  per test
            tmp(k) = Current_all{i}{j}{2,k};            
        end     
        
        % Compute median CI
        tmp = sort(tmp);
        medianCI = [tmp(LB) tmp(UB)];
                
        % Store average Current draw metric in perf_results
        PerfIndicators{i}{j}(2,1) = tmp(UB);      % UB of the 95% CI on the median across tests    
        PerfIndicators{i}{j}(2,2) = 1-tmp(UB)/25; % Normalized performance index
    end    
end

%% Investigation of worst-case current draw

% Extract current draw per node
for i=1:numel(configuration)
    
    nodeList = sort(Current_all{i}{1}{1,1}(:,1));    
    
    for j=1:numel(protocol)        
        
        if do_plots
            figure
        end
        
        % Initialize the storing array
        perNodeCurrent = zeros(numel(nodeList),size(Current_all{i}{j},2));
        
        for k=1:size(Current_all{i}{j},2) % for each test
            
            % Get all current draw
            tmp = Current_all{i}{j}{1,k};
            
            for l=1:numel(nodeList) % for each node
                node=nodeList(l);
                % Get the corresponding current draw and store
                perNodeCurrent(l,k) = tmp( tmp(:,1)==node , 2 );
            end
        end     
        
        % Store in Current_perNode
        Current_perNode{i}{j} = perNodeCurrent';
       
        % Plot current draw per node for protocol j
        if do_plots
            y = ones(1,size(Current_all{i}{j},2));
            for l=1:numel(nodeList) % for each node
                scatter(subplot(numel(nodeList),1,l),perNodeCurrent(l,:),y,'filled','d');
                ylabel(protocol{j});
                xlim([0 25]);
                ylim([0 2]);
                ylabel(num2str(nodeList(l)));
            end
            xlabel('Median current draw [mA]');      
        end
        
        % Compute the median CI for each node
        max_value = 0;
        max_node = 0;
        for l=1:numel(nodeList) % for each node
            tmp = sort(perNodeCurrent(l,:));
            medianCI = [tmp(LB) tmp(UB)];
            if tmp(UB) > max_value                
                max_value = tmp(UB);
                max_node = nodeList(l);
            end
        end
        
        % Store worst-case Current draw metric in perf_results
        PerfIndicators{i}{j}(3,1) = max_value;        % UB of the 95% CI on the median across tests    
        PerfIndicators{i}{j}(3,2) = 1-max_value/25;   % Normalized performance index
        
    end    
end

%% Print performance report
for i=1:numel(configuration)
    
    fprintf('\n%s\n', '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
    fprintf('%s\t%s\n', 'Configuration :', configuration{i});
    fprintf('%s\n', '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
    
    for j=1:numel(protocol)
        
        PRR_indicator(i,j)          = PerfIndicators{i}{j}(1,2);
        Avg_Current_indicator(i,j)  = PerfIndicators{i}{j}(2,2);
        WC_Current_indicator(i,j)   = PerfIndicators{i}{j}(3,2);
                
        fprintf('\nProtocol:\t\t%s', protocol{j});  
        fprintf('\t\tNormalized performance index (higher is better)\n');
        fprintf('Average current draw:\t%2.2f mA ', PerfIndicators{i}{j}(2,1));
        fprintf('\t%2.2f\n',                        PerfIndicators{i}{j}(2,2));
        fprintf('Worst current draw:\t%2.2f mA ',   PerfIndicators{i}{j}(3,1));
        fprintf('\t%2.2f\n',                        PerfIndicators{i}{j}(3,2));        
        fprintf('PRR:\t\t\t%2.2f %%',               PerfIndicators{i}{j}(1,1));
        fprintf('\t\t%2.2f\n',                      PerfIndicators{i}{j}(1,2));
    end    
end

if do_plots
    figure
    scatter3(Avg_Current_indicator(1,:),WC_Current_indicator(1,:),PRR_indicator(1,:), ...
        'filled','o',...
        'MarkerFaceColor',[255,99,46]/255)
    xlim([0 1]);
    ylim([0 1]);
    zlim([0 1]);
    xlabel('Average Energy');
    ylabel('Maximal Energy');
    zlabel('Reliability');
    
    x0=10;
    y0=10;
    width=350;
    height=300;
    set(gcf,'position',[x0,y0,width,height])

    saveas(gcf,'plots/PerfIndicators_all.fig')
    saveas(gcf,'plots/PerfIndicators_all.pdf')
    
    %--PLOTLY--%
    %fig2plotly()
end