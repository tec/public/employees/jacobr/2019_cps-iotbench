#!/bin/bash

# Copyright (c) 2018-2019, Swiss Federal Institute of Technology (ETH Zurich)
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
# 
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
# 
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Script that generates the FlockLab test scripts

# Verify that Flocklab USERNAME and PASSWORD are available
if [ -f .flocklabauth ]
then
  source ./.flocklabauth
else
  if [ -f $HOME/.flocklabauth ]
  then
    source $HOME/.flocklabauth
  else
    if [ $# -ne 2 ]
    then
      # If not, provide USERNAME and PASSWORD at first use
      echo 'First usage: ./generate_flocklab_tests USERNAME PASSWORD'
      exit 1;
    else
      # Save Flocklab account authentification info
      echo "USER=$1" >> .flocklabauth
      echo "PASSWORD=$2" >> .flocklabauth
    fi
  fi
fi

# datarate / duration
datarates='10/40' # 200 * 1/r + 10s + 10s

# first node is sink
topologies='022 002 004 008 015 003 031 032 033 006 016 001 028 018 010'

for i in `find . -type d | sed '/^\.\/[0-9]\+$/!d'` 
do
    for d in $datarates
    do
        for n in `echo $topologies | tr ' ' '_' | tr '.' ' '`
        do
            # Extract information
            datarate=`echo $d | sed 's/\/.*//'`
            duration=`echo $d | sed 's/.*\///'`
            group=`echo $i | sed 's/[^0-9]*//g'`
            nodes=`echo $n | tr '_' ' ' | sed 's/^ //;s/ $//'`
            sinkobs=`echo $nodes | sed 's/ .*//'`
            nodeids=$nodes
            sinkaddress=`echo $nodeids | sed 's/ .*//'`
            configfile=$i/flocklab_dr${datarate}_sink${sinkobs}.xml
            echo "Group: $group, datarate: $datarate, duration: $duration s, sinknode: $sinkobs, nodes: [$nodes]"
 
			      # Generate the Flocklab config file
			      base64 $i/main.dpp-cc430 > /tmp/main.dr.b64
			      sed -n '1h;1!H;${ g;s/<data>[^<]*<\/data>/<data_new>\n<\/data>/;p}' flocklab_template.xml > $configfile
			      sed -i '/<data_new>/r /tmp/main.dr.b64' $configfile
			      sed -i 's/<data_new>/<data>/' $configfile
			      rm  /tmp/main.dr.b64

            # Update fields in FlockLab config file
            sed -i 's/<durationSecs>[^<]*<\/durationSecs>/<durationSecs>'"$duration"'<\/durationSecs>/' $configfile
            sed -i 's/<durationMillisecs>[^<]*<\/durationMillisecs>/<durationMillisecs>'"$duration"'000<\/durationMillisecs>/' $configfile
            sed -i 's/DATARATE=.*/DATARATE='"$datarate"'/' $configfile
            sed -i 's/GROUP=.*/GROUP='"$group"'/' $configfile
            sed -i 's/SINK=.*/SINK='"$sinkobs"'/' $configfile
            sed -i 's/TOPOLOGY=.*/TOPOLOGY='"$nodes"'/' $configfile
            sed -i 's/<!--NODES-->/'"$nodes"'/' $configfile
            sed -i 's/<!--NODEIDS-->/<targetIds>'"$nodeids"'<\/targetIds>/' $configfile
            
            # Submit FlockLab test
            ./flocklab -c $configfile >> test_summary.lst
        done
    done
done

